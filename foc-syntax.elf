%{ == Syntax == }%

pol: type. %name pol P.
+: pol.
~: pol.
*: pol.

%{ We define propositions in an environment with free atoms. }%
atom: pol -> type. %name atom Q q.
%block atom+: block {Q+: atom +}.
%block atom~: block {Q~: atom ~}.

typ: pol -> type. %name typ T.
c: atom P -> typ P.
↓: typ ~ -> typ +.
↑: typ + -> typ ~.
0: typ +.
⊕: typ + -> typ + -> typ +. %infix none 5 ⊕.
1: typ +.
⊗: typ + -> typ + -> typ +. %infix none 5 ⊗.
⊤: typ ~.
&: typ ~ -> typ ~ -> typ ~. %infix none 5 &.
⊸: typ + -> typ ~ -> typ ~. %infix none 5 ⊸.

%{ Right-stable propositions are ones that are either atomic or shifted. }%
stable: typ ~ -> type. %name stable St.
aQ: stable (c Q).
a↑: stable (↑ A+).

%{ An inversion or focus context Δ is a list of positive propositions. }%
pos: type. %name pos _Δ.
·: pos.
,: typ + -> pos -> pos. %infix right 3 ,.
